<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Saved Successfully</name>
   <tag></tag>
   <elementGuidId>23f02cb7-0da0-416e-bb58-21a4d736286d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ajs-message.ajs-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='save site settings'])[1]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7e5f2fee-098e-46dd-b8fc-6db256dce25a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ajs-message ajs-success</value>
      <webElementGuid>7838d19f-6b45-4f1b-b1e3-43ff4980dfc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Saved Successfully</value>
      <webElementGuid>c673ddc0-353c-4fcc-abf4-64c7e4f23c1b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;alertify-notifier ajs-bottom ajs-right&quot;]/div[@class=&quot;ajs-message ajs-success&quot;]</value>
      <webElementGuid>a7e3536d-6ca3-4ac7-b714-18b139e528ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='save site settings'])[1]/following::div[4]</value>
      <webElementGuid>b04a913d-e9dd-4658-9e4a-5186f8c3089c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='cancel'])[1]/following::div[4]</value>
      <webElementGuid>3a2fe9e8-4c37-48a7-acbe-6335c0274712</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Saved Successfully']/parent::*</value>
      <webElementGuid>a572368b-b7d4-4ebd-a0a7-22084290bbdf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>075d0f82-0f4f-4b6e-9c1f-86985deed9be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Saved Successfully' or . = 'Saved Successfully')]</value>
      <webElementGuid>f86a1e6f-3f4e-4227-875c-611c77e46606</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
