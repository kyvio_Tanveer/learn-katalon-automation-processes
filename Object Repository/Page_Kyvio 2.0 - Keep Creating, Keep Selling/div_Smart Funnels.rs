<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Smart Funnels</name>
   <tag></tag>
   <elementGuidId>84005cb7-1fa1-48b0-bf1e-8db909020fa8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.react-dropdown-select-content.react-dropdown-select-type-single.css-v1jrxw-ContentComponent.e1gn6jc30</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9d9a7881-ec95-4fb5-bc0c-0a816d08066e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30</value>
      <webElementGuid>af1cf141-2243-4eb3-a5cf-04b25bfcc805</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Smart Funnels</value>
      <webElementGuid>1b49d83e-daf9-4a63-aa34-47e3c740a356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;sidebar&quot;]/div[@class=&quot;sidebar-select module-selector&quot;]/div[1]/div[@class=&quot;react-dropdown-select form-control custom-dropdown-select full-width undefined  not-multi-drop-down   text-transform-capitalize  css-18f8t8v-ReactDropdownSelect e1gzf2xs0&quot;]/div[@class=&quot;react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30&quot;]</value>
      <webElementGuid>ac267da8-665e-420d-ad7a-79b01e4ab570</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/div/div</value>
      <webElementGuid>978fd6b3-9811-4833-a905-ef18778dcd7a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Funnel Overview'])[1]/preceding::div[2]</value>
      <webElementGuid>74f8ee80-d2fc-4d89-a242-196c2c2e2389</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]/div/div/div</value>
      <webElementGuid>4caf4067-f40b-4f1e-8150-3740250eff5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Smart Funnels' or . = 'Smart Funnels')]</value>
      <webElementGuid>c081ff95-9d8d-4c64-9b02-9999c4d498c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
