import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Core/Login/TC_01 Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_New Funnel'))

WebUI.delay(3)

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Choose funnel type'), 
    'app.choose-funnel-type')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Sales Funnel'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Next'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Customize Sales Funnel'), 
    'app.customize-funnel Sales Funnel')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Next'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Select Template'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Next'))

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Funnel Name_name'), 'autofunnel')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Build your funnel'), 
    'app.build-your-funnel')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Create Funnel'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Kyvio Builder v2/img'), 0)

WebUI.click(findTestObject('Object Repository/Page_Kyvio Builder v2/img'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_autofunnel'), 
    'autofunnel')

WebUI.closeBrowser()

