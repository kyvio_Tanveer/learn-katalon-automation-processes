<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_NEXT STEP</name>
   <tag></tag>
   <elementGuidId>8ab56d39-17cf-48d8-aede-aacd82dae8ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.mr-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-settings-tabs-tabpane-PRODUCT_TYPE']/div/div[3]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>05c8db58-fe27-4972-8008-f90053ea4c93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary mr-4</value>
      <webElementGuid>8cd3e965-3cb8-4fe0-9fcc-0c8be3562eb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>NEXT STEP</value>
      <webElementGuid>523f8095-51ba-4a92-a31d-016d441e16fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-settings-tabs-tabpane-PRODUCT_TYPE&quot;)/div[@class=&quot;product-type mt-4&quot;]/div[@class=&quot;text-right mt-4&quot;]/button[@class=&quot;btn btn-primary mr-4&quot;]</value>
      <webElementGuid>2b0b1cd7-6934-4767-ad2e-8804c9551c79</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-settings-tabs-tabpane-PRODUCT_TYPE']/div/div[3]/button</value>
      <webElementGuid>af8cce9a-3d6f-4752-8a13-085a4530f0f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clothing, Electronics, etc'])[1]/following::button[1]</value>
      <webElementGuid>9f4d92a4-c22e-4e28-8226-c4fb45fa380a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Physical product'])[1]/following::button[1]</value>
      <webElementGuid>dfbe80e9-0be3-4c83-984f-1c446450d935</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/999999'])[1]/preceding::button[1]</value>
      <webElementGuid>c4207058-e1a1-4b37-b8d4-df3736999dc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active'])[1]/preceding::button[1]</value>
      <webElementGuid>40a4da48-ff22-4b60-b67d-29a98ef3d293</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='NEXT STEP']/parent::*</value>
      <webElementGuid>e1195319-b3c2-42bb-8d4d-3fb9121cd874</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>1c8029c7-d5e5-4dae-bb88-218deeb6ac6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'NEXT STEP' or . = 'NEXT STEP')]</value>
      <webElementGuid>639e00fc-946f-445b-b60d-a72293e3dbe4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
