<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Kyvio - Create New Site</name>
   <tag></tag>
   <elementGuidId>8a6f7478-f6a5-4e6b-8668-503b3af9a639</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div[class=&quot;page-title mb-4 capitalize&quot;] h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[3]/main/div/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>f47a7c72-1d5a-46eb-b780-2ea455792b13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kyvio - Create New Site</value>
      <webElementGuid>498f8762-d636-42df-b58e-1264642d7416</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;content&quot;]/main[1]/div[@class=&quot;section-padding pb-5&quot;]/div[@class=&quot;page-title mb-4 capitalize&quot;]/h1[1]</value>
      <webElementGuid>2b661875-6c5c-43de-ad2f-907c7ffd98e2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/main/div/div/h1</value>
      <webElementGuid>9489cd27-5b53-4618-a891-6f659a132a99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[1]/following::h1[1]</value>
      <webElementGuid>4ec536f9-487f-4a0e-95c9-720092fc084c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kyvio API'])[1]/following::h1[1]</value>
      <webElementGuid>c58b2c0f-8e58-4521-9903-819c8e6cef2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='site name'])[1]/preceding::h1[1]</value>
      <webElementGuid>ac95b83c-6385-4f9b-bf38-2f2f61c5796b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Site URL'])[1]/preceding::h1[1]</value>
      <webElementGuid>1ae63c64-317f-408c-846f-645b22d89af4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kyvio']/parent::*</value>
      <webElementGuid>726e755d-b57e-4969-98f5-5dfb66279e27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>0000b241-2eb1-4b99-bef6-49854551e01f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Kyvio - Create New Site' or . = 'Kyvio - Create New Site')]</value>
      <webElementGuid>d2dbc30e-5981-4b35-934a-73e05fbb0303</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
