<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Thank you page_react-dropdown-select-_fa60d2</name>
   <tag></tag>
   <elementGuidId>ff98d69a-c1ab-4af7-ae16-f91bddc47429</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.react-dropdown-select.form-control.custom-dropdown-select.full-width.undefined.not-multi-drop-down.w-full.text-transform-capitalize.css-18f8t8v-ReactDropdownSelect.e1gzf2xs0 > div.react-dropdown-select-content.react-dropdown-select-type-single.css-v1jrxw-ContentComponent.e1gn6jc30 > input.react-dropdown-select-input.css-o79eln-InputComponent.e11wid6y0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a93d79a3-6faf-4dae-9d99-cbf8696954f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>1ffa0659-3fec-4234-b899-23be0ff6370b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>react-dropdown-select-input css-o79eln-InputComponent e11wid6y0</value>
      <webElementGuid>9d5f31a1-216b-4f83-b7f7-c7f750c7790e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>size</name>
      <type>Main</type>
      <value>9</value>
      <webElementGuid>45b38b3e-8216-46c6-a0e5-428088efe731</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Select...</value>
      <webElementGuid>34dd62ba-6a91-4b18-83e8-2c1c010ca1db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-1&quot;)/div[@class=&quot;card-body collapse in show&quot;]/form[1]/div[@class=&quot;card-body collapse show&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-sm-9&quot;]/div[1]/div[@class=&quot;react-dropdown-select form-control custom-dropdown-select full-width undefined  not-multi-drop-down w-full  text-transform-capitalize  css-18f8t8v-ReactDropdownSelect e1gzf2xs0&quot;]/div[@class=&quot;react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30&quot;]/input[@class=&quot;react-dropdown-select-input css-o79eln-InputComponent e11wid6y0&quot;]</value>
      <webElementGuid>9859c1e9-2638-43b4-850c-b3548a1c3aa2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[5]</value>
      <webElementGuid>04fec07f-60a1-4dc9-93ed-28af4b06d256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-1']/div[2]/form/div/div[6]/div/div/div/div/input</value>
      <webElementGuid>d3aae4f3-0062-475c-8df5-cd17f9e42c97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div/div/input</value>
      <webElementGuid>c3f84386-8d9b-4c4a-a983-cf248f89c11c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Select...']</value>
      <webElementGuid>e137a8aa-05f5-40a7-812e-013396fdc4d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
