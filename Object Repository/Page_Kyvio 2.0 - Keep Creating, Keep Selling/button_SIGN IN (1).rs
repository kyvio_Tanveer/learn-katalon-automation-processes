<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_SIGN IN (1)</name>
   <tag></tag>
   <elementGuidId>8e4e4805-af2e-4c9a-95cd-d683dc0403c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.float-right.btn-width-sm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>609a3be1-5bbf-4893-8627-8a650dbf8dfb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>36024584-c2e1-4644-9f4a-0a05ac477b3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary float-right btn-width-sm</value>
      <webElementGuid>e44a911f-d360-4971-bbe2-c6a679fd07fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>SIGN IN</value>
      <webElementGuid>ee934e62-886c-45ce-be86-8882dbb1e611</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper login&quot;]/main[1]/div[@class=&quot;d-sm-flex nopadding main-wrapper bg-white&quot;]/div[@class=&quot;login-form-holder col-sm-8&quot;]/div[@class=&quot;small-content&quot;]/div[@class=&quot;mt-4&quot;]/form[1]/div[@class=&quot;clearfix&quot;]/button[@class=&quot;btn btn-primary float-right btn-width-sm&quot;]</value>
      <webElementGuid>8f227c43-05b9-4744-87d8-3def98673676</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>19915a52-5ace-4946-91bb-b244f8499ac6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div/div/div/div/form/div[3]/button</value>
      <webElementGuid>f81bde9e-d987-4684-bd12-dc40cfeac258</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PASSWORD'])[1]/following::button[1]</value>
      <webElementGuid>e702666d-38e3-44b9-9f84-e3c1f0c76368</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EMAIL ADDRESS'])[1]/following::button[1]</value>
      <webElementGuid>32cbb591-b56c-4673-ba9d-f57da416e192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Signup Today'])[1]/preceding::button[1]</value>
      <webElementGuid>e2bf9897-9920-48ca-8eb9-82ebe112c0a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About Us'])[1]/preceding::button[1]</value>
      <webElementGuid>5eef34ee-a71e-4d9d-a003-3cab46e27267</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='SIGN IN']/parent::*</value>
      <webElementGuid>c62a5c56-2ae7-4bad-8f87-db2da0dfa034</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>460397a3-b76b-4693-b7d9-af4e7cd59252</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'SIGN IN' or . = 'SIGN IN')]</value>
      <webElementGuid>6148263c-ea2a-4d8f-8f1a-291e2ef9ee30</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
