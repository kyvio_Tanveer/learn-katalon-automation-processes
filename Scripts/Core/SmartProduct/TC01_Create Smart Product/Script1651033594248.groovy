import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Core/Login/TC_01 Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Smart Funnels'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Smart Products'), 
    'App.Smart-Products')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Smart Products'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/h1_Smart Products - Dashboard'), 
    'app.sp-dashboard')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Product Overview'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_New Product'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/h1_Create New Product'), 
    'app.create-new-product')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Membership productVideo courses, training etc'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_NEXT STEP'))

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Product name_productName'), 
    'autoprod')

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Product Support Link_productSupportLink'), 
    'http://autoprod.icu')

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Cancellation  Refund Policy URL_cance_ed6434'), 
    'http://autoprod.icu/refund-policy')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_NEXT STEP_1'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/h4_Accepted Payment Methods'), 
    'app.accepted-payment-method')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_ADD PRICE VARIANT'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/p_Free  - Price 0'))

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Name_priceName'), 'basic')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Thank you page_react-dropdown-select-_fa60d2'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_pop-up test thank-you-page'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_pop-up test thank-you-page_1'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Custom Link'))

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Custom Link_customThankYouPageUrl'), 
    'www.autopro.icu/thankyou')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_NEXT STEP'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_NEXT STEP'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Create Product'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_autoprod'), 
    'autoprod')

WebUI.closeBrowser()

