<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Select Template</name>
   <tag></tag>
   <elementGuidId>c71c7ab9-3858-4af7-ac61-1c80b1a4b9b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.select-text.pointer.capitalize.no-button-look</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/following::button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>23145852-dfe0-4b49-9df5-7a9474b1b4f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select-text pointer capitalize no-button-look</value>
      <webElementGuid>d5d811fb-8d1b-4f50-ad30-3080da853bc0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select Template</value>
      <webElementGuid>59e98638-8ba9-48f4-81a7-704c31d900bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_body__e43Pz modal-body&quot;]/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;row width-100&quot;]/div[@class=&quot;col-lg-4 col-md-4 col-xs-6 template funnel-template mb-4 false&quot;]/div[@class=&quot;d-block text-center pos-relative&quot;]/div[@class=&quot;box-shadow radius-sm  mb-3 thumbnail&quot;]/div[@class=&quot;overlay&quot;]/button[@class=&quot;select-text pointer capitalize no-button-look&quot;]</value>
      <webElementGuid>86e2836a-ae5c-46fd-a18a-1f3cc91e3fa4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/following::button[1]</value>
      <webElementGuid>3939a104-9147-449c-afc5-33adb4cc1783</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::button[1]</value>
      <webElementGuid>9529baae-73bd-4cb9-9482-8e4db81b31f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[2]/preceding::button[1]</value>
      <webElementGuid>a7ae18a8-c91b-4d14-bdee-06d2eb01a773</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Template'])[2]/preceding::button[1]</value>
      <webElementGuid>40df590e-2bc6-44e1-99c1-4e993c7c8bf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select Template']/parent::*</value>
      <webElementGuid>a697bee4-afc2-415c-a7ea-6414964b74b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/div/div/div/div/button</value>
      <webElementGuid>815f095b-62c4-4c3e-9aae-1f09f5d71eb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Select Template' or . = 'Select Template')]</value>
      <webElementGuid>80a7f3bf-8cb7-4476-8cbe-f81cfd496e91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
