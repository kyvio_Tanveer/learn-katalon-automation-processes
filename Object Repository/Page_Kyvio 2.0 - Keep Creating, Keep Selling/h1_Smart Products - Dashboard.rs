<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Smart Products - Dashboard</name>
   <tag></tag>
   <elementGuidId>241063e6-71c2-410b-9199-fad7b8f8ca7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/main/div/div/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>9198e70e-fe33-4765-ae59-bdeafcf6e810</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Smart Products - Dashboard</value>
      <webElementGuid>2566ae7d-58ab-4a0e-962c-b4a0a21fc06d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;content&quot;]/main[@class=&quot;section-padding&quot;]/div[1]/div[@class=&quot;border-line&quot;]/div[@class=&quot;page-title&quot;]/h1[1]</value>
      <webElementGuid>4329dfe2-df7d-43be-9445-d0b0f8f0ae45</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/main/div/div/div/h1</value>
      <webElementGuid>72df8b95-74cf-401d-bb4b-4d191fa73275</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[1]/following::h1[1]</value>
      <webElementGuid>c2407771-18a4-4cf9-b4b2-98ff8a7cfca7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kyvio API'])[1]/following::h1[1]</value>
      <webElementGuid>3d983a89-21b2-4f66-a010-120347385d2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='£0'])[1]/preceding::h1[1]</value>
      <webElementGuid>82e1881d-03cd-4664-99c5-dda36c173df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EUR'])[1]/preceding::h1[1]</value>
      <webElementGuid>3ed2ee23-a481-45a7-9f03-8260cfe235e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Smart Products - Dashboard']/parent::*</value>
      <webElementGuid>debe05e0-1049-42f9-b58b-ba0f6767f130</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>482810c6-fb41-4f8a-902c-7847c95a2b19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Smart Products - Dashboard' or . = 'Smart Products - Dashboard')]</value>
      <webElementGuid>0d569184-2ce3-4626-bb0e-01b0d263380d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
