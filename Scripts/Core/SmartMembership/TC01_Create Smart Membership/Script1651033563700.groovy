import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Core/Login/TC_01 Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Smart Funnels'))

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/div_Smart FunnelsDashboard Smart Funnels Sm_4e4791'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Smart Memberships'), 
    'App.Smart-Memberships')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Smart Memberships'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Smart Memberships_1'), 
    'App.Smart-Memberships')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Members Manager'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/a_Add Member'), 'App.Add-Member')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/a_Add Member'))

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Email Address_emailAddressMemberShip'), 
    'adam@sm.icu')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_PASSWORD_password'), 
    'RigbBhfdqOAzyPoZeICXsA==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_Confirm Password_confirmPassword'), 
    'RigbBhfdqOAzyPoZeICXsA==')

WebUI.setText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/input_First Name_firstName'), 
    'Adam')

WebUI.click(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/button_Save Member'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/h1_Smart Memberships - Member Manager'), 
    'app.sm-member-manager')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Kyvio 2.0 - Keep Creating, Keep Selling/span_Adam'), 'Adam')

WebUI.closeBrowser()

