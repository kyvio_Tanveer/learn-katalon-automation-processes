<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sales Funnel (1)</name>
   <tag></tag>
   <elementGuidId>d45ca122-493d-4143-99c3-4d35511134c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.radio-info.styles_typeItem__2QDN8</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3d4947de-8ee4-443e-b8c9-82565693af23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>radio-info styles_typeItem__2QDN8</value>
      <webElementGuid>05b0aed8-864d-4367-a6a4-d1166fd989f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sales Funnel</value>
      <webElementGuid>91260981-c1ff-4082-81a2-47519230a26e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_body__e43Pz modal-body&quot;]/div[@class=&quot;styles_typesList__vOt8K&quot;]/label[@class=&quot;radio-label funnel-type-button styles_typeItemLabel__3ckW6&quot;]/div[@class=&quot;radio-info styles_typeItem__2QDN8&quot;]</value>
      <webElementGuid>a21cc99a-2ff5-4f50-a486-9b361eca0963</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[3]</value>
      <webElementGuid>4b00ff21-2f93-45fb-8147-b391f9ccd84b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose funnel type'])[1]/following::div[3]</value>
      <webElementGuid>6b3731f8-36f0-46a5-b2bf-50a4cd4c6541</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kyvio BETA'])[1]/preceding::div[5]</value>
      <webElementGuid>cf0fb17d-b6fc-4492-8377-1039578dd5fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[2]/preceding::div[6]</value>
      <webElementGuid>382c6c10-3a4a-4618-8c27-8595ba50a934</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/div</value>
      <webElementGuid>4feff645-259a-4e8f-9ef1-f19abe7815e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Sales Funnel' or . = 'Sales Funnel')]</value>
      <webElementGuid>67205717-27d9-4ca3-943f-1b84ef61e5a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
