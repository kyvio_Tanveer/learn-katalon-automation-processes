<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_pop-up test thank-you-page_1</name>
   <tag></tag>
   <elementGuidId>5373ea86-a298-4801-87f2-6c70c44a4b8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-9 > div > div.react-dropdown-select.form-control.custom-dropdown-select.full-width.undefined.not-multi-drop-down.text-transform-capitalize.css-18f8t8v-ReactDropdownSelect.e1gzf2xs0 > div.react-dropdown-select-content.react-dropdown-select-type-single.css-v1jrxw-ContentComponent.e1gn6jc30 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='list-1']/div[2]/form/div/div[6]/div/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5e25a1c0-9151-4dd7-8080-73d631ceb5e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>pop-up test thank-you-page </value>
      <webElementGuid>3772c88b-06d8-4f25-8eca-2c95428268bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-1&quot;)/div[@class=&quot;card-body collapse in show&quot;]/form[1]/div[@class=&quot;card-body collapse show&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-sm-9&quot;]/div[1]/div[@class=&quot;react-dropdown-select form-control custom-dropdown-select full-width undefined  not-multi-drop-down   text-transform-capitalize  css-18f8t8v-ReactDropdownSelect e1gzf2xs0&quot;]/div[@class=&quot;react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30&quot;]/span[1]</value>
      <webElementGuid>893771e6-b0b3-4c83-b3d4-e6689ea25127</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='list-1']/div[2]/form/div/div[6]/div/div/div/div/span</value>
      <webElementGuid>402f64eb-5c31-4784-9b90-6e21a543633e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thank you page *'])[1]/following::span[1]</value>
      <webElementGuid>2e2d331d-abdd-479a-98f5-0e3181aa9281</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Currency *'])[1]/following::span[1]</value>
      <webElementGuid>12dce172-f4e7-4de4-8ad4-c6f4c5f0a52a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Stock (Optional)'])[1]/preceding::span[1]</value>
      <webElementGuid>241eb0c0-2e41-489e-88ef-16daff5d30d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unlimited or limited quantity'])[1]/preceding::span[2]</value>
      <webElementGuid>b3d8c210-6e30-46be-9090-049d7982fc6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='pop-up test thank-you-page']/parent::*</value>
      <webElementGuid>873b7f2e-cd99-4bc5-8355-c44bf514f1d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div/div/span</value>
      <webElementGuid>4c4df32a-aee3-4fc8-9bd6-8be68301da3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'pop-up test thank-you-page ' or . = 'pop-up test thank-you-page ')]</value>
      <webElementGuid>1baf0976-59c5-4cf0-8994-f39fd6ac75d1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
