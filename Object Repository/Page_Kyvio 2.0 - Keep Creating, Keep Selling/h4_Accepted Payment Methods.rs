<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Accepted Payment Methods</name>
   <tag></tag>
   <elementGuidId>c25786ae-5cf4-4d58-880a-790f4e89ceae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h4.title-section.mb-4.mt-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-settings-tabs-tabpane-PRODUCT_PRICING']/div/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>548a2c3c-d507-45c9-befc-6a7135447325</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title-section mb-4 mt-4</value>
      <webElementGuid>f101ed81-9df9-4f19-b075-c6b4098af85e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Accepted Payment Methods</value>
      <webElementGuid>a1fb393f-bfb1-4bf4-827c-a5ae91f5a864</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-settings-tabs-tabpane-PRODUCT_PRICING&quot;)/div[@class=&quot;payment-pricing&quot;]/h4[@class=&quot;title-section mb-4 mt-4&quot;]</value>
      <webElementGuid>ef89d208-5bc9-4a02-8503-c5804a0f7754</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-settings-tabs-tabpane-PRODUCT_PRICING']/div/h4</value>
      <webElementGuid>11d684ef-48db-4ec7-adce-c841109100eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create New Product'])[1]/following::h4[1]</value>
      <webElementGuid>420c3b15-275b-4cf0-8c36-cd989ac46384</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 5: Delivery'])[1]/following::h4[1]</value>
      <webElementGuid>bfcbcf30-ecf7-4cfa-bc7b-f2bd707ac000</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='please add a payment configuration'])[1]/preceding::h4[1]</value>
      <webElementGuid>c28f8504-1689-4435-a854-a7d5e078025d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='click here to reload your integrations'])[1]/preceding::h4[1]</value>
      <webElementGuid>1ddbfc38-7843-4953-acfe-537d92837cd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Accepted Payment Methods']/parent::*</value>
      <webElementGuid>fe2acdb3-4ff1-44a4-92c0-2989ab86a7c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h4</value>
      <webElementGuid>8f06681d-2b32-47d7-b3f7-28b9df0d308d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Accepted Payment Methods' or . = 'Accepted Payment Methods')]</value>
      <webElementGuid>ef363d13-9846-4379-96ba-b7ec9d98dfd8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
