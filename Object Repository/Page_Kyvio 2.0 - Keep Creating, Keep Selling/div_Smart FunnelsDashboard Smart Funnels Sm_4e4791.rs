<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Smart FunnelsDashboard Smart Funnels Sm_4e4791</name>
   <tag></tag>
   <elementGuidId>2fb17ca1-0b5d-4bd4-b96e-fce78111b7fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.react-dropdown-select.form-control.custom-dropdown-select.full-width.undefined.not-multi-drop-down.text-transform-capitalize.css-18f8t8v-ReactDropdownSelect.e1gzf2xs0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>41139cdc-0126-4704-a31c-4ec577169fbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>9afe7989-b79a-4636-ba40-b4bb572878ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>direction</name>
      <type>Main</type>
      <value>ltr</value>
      <webElementGuid>dc7e5678-6224-484e-bc03-cb942420d4a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>react-dropdown-select form-control custom-dropdown-select full-width undefined  not-multi-drop-down   text-transform-capitalize  css-18f8t8v-ReactDropdownSelect e1gzf2xs0</value>
      <webElementGuid>0d8cba8f-8058-481e-b36c-87e16e1fe038</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>color</name>
      <type>Main</type>
      <value>#b1cf5d</value>
      <webElementGuid>5fc11cda-1f46-495c-b952-05b07d77f4b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Smart FunnelsDashboard Smart Funnels Smart Memberships Smart Products Smart Journeys Support </value>
      <webElementGuid>f93e91ec-0970-4b66-a6c8-3702e85de448</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;sidebar&quot;]/div[@class=&quot;sidebar-select module-selector&quot;]/div[1]/div[@class=&quot;react-dropdown-select form-control custom-dropdown-select full-width undefined  not-multi-drop-down   text-transform-capitalize  css-18f8t8v-ReactDropdownSelect e1gzf2xs0&quot;]</value>
      <webElementGuid>d11863cc-3976-4585-aa79-677405a3d2ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div[2]/div/div</value>
      <webElementGuid>b69ff3e8-6431-451d-a32c-0d33a68dec8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]/div/div</value>
      <webElementGuid>1f81a91b-307d-4e1b-a4e1-dcf3b8e49869</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Smart FunnelsDashboard Smart Funnels Smart Memberships Smart Products Smart Journeys Support ' or . = 'Smart FunnelsDashboard Smart Funnels Smart Memberships Smart Products Smart Journeys Support ')]</value>
      <webElementGuid>31238a40-5704-4dc2-9a6e-f75a49cec977</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
