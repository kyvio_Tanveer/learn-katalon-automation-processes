<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Welcome back, John</name>
   <tag></tag>
   <elementGuidId>a8659624-cf24-47f9-8668-e4537b2b6df7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.font-22.font-semibold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[3]/main/div/ul/div/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>853c1ef3-4bc1-4496-9186-93577e5dd546</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>font-22 font-semibold</value>
      <webElementGuid>e4d1cbe4-844a-474a-ad39-6bb29a0b1abd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Welcome back, John</value>
      <webElementGuid>1f8cd01d-453c-49d2-b86c-444464e4540a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;content&quot;]/main[1]/div[@class=&quot;section-padding&quot;]/ul[@class=&quot;flex-3-items list-unstyled&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row section-margin&quot;]/div[@class=&quot;col-lg-12&quot;]/div[@class=&quot;card-dashboard extra-pad welcome&quot;]/h2[@class=&quot;font-22 font-semibold&quot;]</value>
      <webElementGuid>9e196f4b-1cbe-4be7-b8ea-f380ec450699</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/main/div/ul/div/div/div/div/h2</value>
      <webElementGuid>7cb25d3e-575b-460f-a34b-3ed9d4e72726</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[1]/following::h2[1]</value>
      <webElementGuid>776f9b8c-9d90-4d1b-b130-ce87c42c8799</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kyvio API'])[1]/following::h2[1]</value>
      <webElementGuid>f67cce08-c8a8-459e-bff5-8b5435897cc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Today is a great day to land some new clients'])[1]/preceding::h2[1]</value>
      <webElementGuid>55468e73-a919-41cb-9783-f7328873ca04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='see more'])[1]/preceding::h2[1]</value>
      <webElementGuid>cc36cf09-a3db-4b32-a5ff-a9e6eb5494e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Welcome back,']/parent::*</value>
      <webElementGuid>7e34401a-84ec-4e37-acee-fcd05495ea4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>0873ed9e-256c-4481-a402-ca736b81a490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = ' Welcome back, John' or . = ' Welcome back, John')]</value>
      <webElementGuid>4407f334-fcb4-4186-a3e6-2813189ffaa1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
