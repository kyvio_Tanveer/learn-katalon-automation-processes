<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Select Template (1)</name>
   <tag></tag>
   <elementGuidId>1d40809c-ba67-416b-8829-20dae35ec0c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.select-text.pointer.capitalize.no-button-look</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/following::button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f433ac1b-299f-476b-88b2-d112c7ea4830</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select-text pointer capitalize no-button-look</value>
      <webElementGuid>11a37446-f7ee-46c6-b964-dafaa9455fe5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Select Template</value>
      <webElementGuid>967b0b48-fdb2-4e3a-b1c5-f7bfb4c27543</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_body__e43Pz modal-body&quot;]/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;row width-100&quot;]/div[@class=&quot;col-lg-4 col-md-4 col-xs-6 template funnel-template mb-4 false&quot;]/div[@class=&quot;d-block text-center pos-relative&quot;]/div[@class=&quot;box-shadow radius-sm  mb-3 thumbnail&quot;]/div[@class=&quot;overlay&quot;]/button[@class=&quot;select-text pointer capitalize no-button-look&quot;]</value>
      <webElementGuid>299a9121-7a0c-4844-b217-a6fb83da04db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/following::button[1]</value>
      <webElementGuid>0b598753-cb96-4301-ba66-b3ba593fdbf1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::button[1]</value>
      <webElementGuid>66d5ee83-df1f-4721-9909-7b0b6419b220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[2]/preceding::button[1]</value>
      <webElementGuid>1a9ad3b1-da0a-49b2-9cf5-051294430526</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Template'])[2]/preceding::button[1]</value>
      <webElementGuid>034eb78f-6578-4d0d-ae1b-d6dbfe12d401</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Select Template']/parent::*</value>
      <webElementGuid>3b18ac92-06e6-4664-910d-c6940b40e016</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/div/div/div/div/button</value>
      <webElementGuid>9930ba5c-00a7-4292-8014-940cfad449dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Select Template' or . = 'Select Template')]</value>
      <webElementGuid>d300a3f6-9fda-4631-850f-010d5ddaf76c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
