<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Create New Product</name>
   <tag></tag>
   <elementGuidId>f0241303-3cb6-4ff3-a716-9947b676074c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.section-padding.first-product > div.section-padding.border-line > div.page-title.right-abs-btn > h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/main/div[2]/div/div/div/div[2]/div/div/div/div/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>dc9204ca-401d-48e5-a776-ea7c02370740</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create New Product</value>
      <webElementGuid>89ad1412-ddc7-42ca-99ed-47d5c6710bb3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;content&quot;]/main[1]/div[@class=&quot;modal fade popup-styles show active popup-with-sidebar create-product-modal&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content clearfix&quot;]/div[@class=&quot;inside-modal inside-wrapper&quot;]/div[@class=&quot;content-popup tab-content&quot;]/div[@class=&quot;tab-content section-padding pt-4 pr-3&quot;]/div[@class=&quot;mb-30 create-product&quot;]/div[@class=&quot;section-padding first-product&quot;]/div[@class=&quot;section-padding border-line&quot;]/div[@class=&quot;page-title right-abs-btn&quot;]/h1[1]</value>
      <webElementGuid>1d6cccdf-e171-43d7-9d83-34b9b58c46f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/main/div[2]/div/div/div/div[2]/div/div/div/div/div/h1</value>
      <webElementGuid>fc4c29d3-56d1-4211-86a4-e0be33d21f6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 5: Delivery'])[1]/following::h1[1]</value>
      <webElementGuid>a5d856ba-2647-43a1-9ead-1eb70a7a9927</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Step 4: Actions'])[1]/following::h1[1]</value>
      <webElementGuid>eb49923a-7928-48e0-b864-b68443a47e48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Digital product'])[1]/preceding::h1[1]</value>
      <webElementGuid>155c032e-ae9c-4ae0-af46-76d0044e2563</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Softwares, file downloads etc.'])[1]/preceding::h1[1]</value>
      <webElementGuid>dcd95a29-0fd5-48b8-aa25-3b3f900274ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create New Product']/parent::*</value>
      <webElementGuid>09df5bcc-4410-424b-860c-6cc5f6c3f8ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h1</value>
      <webElementGuid>9d4935b6-6beb-44c0-8a15-709e03f34bd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Create New Product' or . = 'Create New Product')]</value>
      <webElementGuid>60f0c731-5dec-4ac0-8970-590cc1378d38</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
