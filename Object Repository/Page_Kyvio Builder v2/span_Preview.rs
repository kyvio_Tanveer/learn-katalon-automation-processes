<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Preview</name>
   <tag></tag>
   <elementGuidId>34deb639-8957-4540-bcee-af18abefaf82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ku-tb-right.ku-tb-part > div.ku-tb-group > div.ku-menu-node > span.ku-mn-content > span.ku-mn-text.ku-mobile-hidden</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='builder_toolbar']/div[3]/div[3]/div/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>47164043-fd75-4482-8740-9e98871a4475</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ku-mn-text ku-mobile-hidden</value>
      <webElementGuid>cb27d824-c7cb-4253-a2d5-5a93c3621238</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Preview</value>
      <webElementGuid>3727c14c-2558-492e-92f7-96ec2399fa18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;builder_toolbar&quot;)/div[@class=&quot;ku-tb-right ku-tb-part&quot;]/div[@class=&quot;ku-tb-group&quot;]/div[@class=&quot;ku-menu-node&quot;]/span[@class=&quot;ku-mn-content&quot;]/span[@class=&quot;ku-mn-text ku-mobile-hidden&quot;]</value>
      <webElementGuid>7797b4b3-3917-4a40-86ee-3dd870d0a07d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='builder_toolbar']/div[3]/div[3]/div/span/span</value>
      <webElementGuid>3170f178-07f0-4800-87fe-db4ae8573a5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[1]/following::span[4]</value>
      <webElementGuid>1a059369-0ae1-46bf-9daf-c4191b2dd551</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Full Screen'])[1]/following::span[6]</value>
      <webElementGuid>cc146b70-7c43-4acb-bd80-7ab992e35443</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Editor'])[1]/preceding::span[1]</value>
      <webElementGuid>fead7855-993b-4803-aa71-72df6993ab54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Publish'])[1]/preceding::span[3]</value>
      <webElementGuid>b7bfdd50-2b55-4728-a8b2-9ece71bf8c6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Preview']/parent::*</value>
      <webElementGuid>1c17ccc8-4a8a-41ec-be6f-123ddcc6ffde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/span/span</value>
      <webElementGuid>1a6e7631-4193-4758-822a-457e350eb0ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Preview' or . = 'Preview')]</value>
      <webElementGuid>f264e028-cd31-4a08-b3a5-3a60f0dd7851</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
