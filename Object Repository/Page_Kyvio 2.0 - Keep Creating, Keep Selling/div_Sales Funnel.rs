<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sales Funnel</name>
   <tag></tag>
   <elementGuidId>c6aeb23f-8c91-4f18-acf9-9b7db16c5764</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.radio-info.styles_typeItem__2QDN8</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>735a88df-236f-4efa-92dd-88ec6fb6b124</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>radio-info styles_typeItem__2QDN8</value>
      <webElementGuid>92122f01-9ccf-4e98-8c44-1d1747fd91f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sales Funnel</value>
      <webElementGuid>89f604ec-2df0-4258-8950-90a47df34482</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_body__e43Pz modal-body&quot;]/div[@class=&quot;styles_typesList__vOt8K&quot;]/label[@class=&quot;radio-label funnel-type-button styles_typeItemLabel__3ckW6&quot;]/div[@class=&quot;radio-info styles_typeItem__2QDN8&quot;]</value>
      <webElementGuid>cf53beca-cd82-42da-9bf3-dcbc8bd7c847</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[3]</value>
      <webElementGuid>2cf43dd8-2589-4536-8f7e-6d3aab18543a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose funnel type'])[1]/following::div[3]</value>
      <webElementGuid>b61a1fed-c89c-443d-8bb1-e64cd2b0f559</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kyvio BETA'])[1]/preceding::div[5]</value>
      <webElementGuid>4d6abd9d-2c27-4f29-950e-6c11fb6f291a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[2]/preceding::div[6]</value>
      <webElementGuid>2aae0816-8c41-45e4-a716-b3d6b8ab110d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/div</value>
      <webElementGuid>b127f3db-5152-4dc7-be49-90680baf287d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Sales Funnel' or . = 'Sales Funnel')]</value>
      <webElementGuid>e3078a42-f60a-4758-b3b5-7e5a27267898</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
