<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Thank You Page</name>
   <tag></tag>
   <elementGuidId>720bbadd-8aba-4b5d-8e34-d516469b92ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-7 > div > div.react-dropdown-select.form-control.custom-dropdown-select.full-width.undefined.not-multi-drop-down.text-transform-capitalize.css-18f8t8v-ReactDropdownSelect.e1gzf2xs0 > div.react-dropdown-select-content.react-dropdown-select-type-single.css-v1jrxw-ContentComponent.e1gn6jc30</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>cfa3ce71-3aae-4464-8c3e-36546f0f9f9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30</value>
      <webElementGuid>25cb9c0f-d768-4003-a837-86392172f625</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Thank You Page</value>
      <webElementGuid>2fe94f71-a477-4a34-9305-7e91552aed9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_body__e43Pz modal-body&quot;]/div[1]/ul[1]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-sm-7&quot;]/div[1]/div[@class=&quot;react-dropdown-select form-control custom-dropdown-select full-width undefined  not-multi-drop-down   text-transform-capitalize  css-18f8t8v-ReactDropdownSelect e1gzf2xs0&quot;]/div[@class=&quot;react-dropdown-select-content react-dropdown-select-type-single css-v1jrxw-ContentComponent e1gn6jc30&quot;]</value>
      <webElementGuid>3bed7fd0-5d7c-4627-adf2-a1c69adb0165</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[7]</value>
      <webElementGuid>42da6b08-4464-4f21-a062-9404c125a3b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div/div/div/div/div</value>
      <webElementGuid>53675ad0-a231-454f-a51b-bde4b09a5cff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Thank You Page' or . = 'Thank You Page')]</value>
      <webElementGuid>b749f68b-8cd7-47c8-abc3-8f8c10a71d0c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
