<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Customize Sales Funnel</name>
   <tag></tag>
   <elementGuidId>5451be82-a863-4f23-a8dd-fd030e1f70ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-title.h4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>80fd5d8d-7fb2-409d-b078-a09c5e9b4b74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-title h4</value>
      <webElementGuid>88537ef0-5ab8-48fe-91ef-bbfae0dda001</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Customize Sales Funnel</value>
      <webElementGuid>4b91ddb8-253e-4a64-a463-e4042d53350b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_header__3_Ct7 modal-header&quot;]/div[@class=&quot;modal-title h4&quot;]</value>
      <webElementGuid>88a8bf0e-92ed-4fa4-8502-98fbc6c10086</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::div[1]</value>
      <webElementGuid>4d21c598-220c-4ca9-b662-1b674026d408</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Customize']/parent::*</value>
      <webElementGuid>615e1ce6-672b-4885-95b6-81dc3554745d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div[2]/div/div/div</value>
      <webElementGuid>2cdfad80-d2dc-4118-ae8e-7308ae679254</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Customize Sales Funnel' or . = 'Customize Sales Funnel')]</value>
      <webElementGuid>13299012-cf58-4b2c-8668-71b1e0298863</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
