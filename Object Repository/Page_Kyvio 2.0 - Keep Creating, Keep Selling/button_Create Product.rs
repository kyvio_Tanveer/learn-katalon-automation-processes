<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create Product</name>
   <tag></tag>
   <elementGuidId>5ae4e154-e5f6-46c0-810e-0310ac32cdc1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-settings-tabs-tabpane-PRODUCT_DELIVERY']/div/div/div/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>eb88ff6c-ced3-40de-9945-7a48c5a43c09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary upper-case</value>
      <webElementGuid>ad55b4dc-271d-453f-9a08-dfb8dfaa16a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Product</value>
      <webElementGuid>49282b8e-f9ba-45b6-9064-9598ffbd9f9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-settings-tabs-tabpane-PRODUCT_DELIVERY&quot;)/div[@class=&quot;tab-pane fade check-change-parent active show&quot;]/div[1]/div[@class=&quot;text-right&quot;]/button[@class=&quot;btn btn-primary upper-case&quot;]</value>
      <webElementGuid>79c09789-9916-4196-908d-0a8b57511af8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-settings-tabs-tabpane-PRODUCT_DELIVERY']/div/div/div/button[2]</value>
      <webElementGuid>8bd89095-defd-4cc4-a721-bd3c46ad4687</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::button[1]</value>
      <webElementGuid>9077140d-95fe-45e9-b77f-e187dbafa296</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Powered by Tiny'])[1]/following::button[2]</value>
      <webElementGuid>2db9303f-837e-472d-9fbb-8e3f9fd715bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/999999'])[1]/preceding::button[1]</value>
      <webElementGuid>ee1d941d-8ca7-4056-8a6e-a76f0e38cd4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active'])[1]/preceding::button[1]</value>
      <webElementGuid>fd16e15b-73b9-4738-9de5-38cae48af114</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create Product']/parent::*</value>
      <webElementGuid>87fe4a87-81d6-4d08-8d40-4bf4d285d4c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/button[2]</value>
      <webElementGuid>59f73241-511b-459e-9ee1-1e5d3eb948d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Create Product' or . = 'Create Product')]</value>
      <webElementGuid>935a7cb1-8dcf-4779-a37a-4a0d6fbdca29</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
