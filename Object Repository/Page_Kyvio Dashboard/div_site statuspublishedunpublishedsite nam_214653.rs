<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_site statuspublishedunpublishedsite nam_214653</name>
   <tag></tag>
   <elementGuidId>e48ae716-2913-4f00-9992-0b7845d953bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ab-content.section-padding.pt-5.pb-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[3]/main/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3e8f766c-6d93-4545-831c-0ae927f8e9b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ab-content section-padding pt-5 pb-5</value>
      <webElementGuid>d0f383ed-f2a8-48e3-a6d2-950eb891d479</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>site statuspublishedunpublishedsite nameSite URL*This site URL can not be changed, you can set your own custom URL using Domain Mapping in the menu above.site logoe.g. https://domain.com/logo-image.pngbrowse / uploadfavicone.g. https://domain.com/favicon.icobrowse / uploadGoogle Sitemap XMLe.g. https://domain.com/sitemap.xmluploadManage Business siteClick here to manage your business site
  	.t5cac70be-c10e-40a6-ba6d-802f2d51e3e6 {
	    color: #fff;
	    background: #222;
	    border: 1px solid transparent;
  	}

  	.t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top {
        margin-top: -10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top::before {
        border-top: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        bottom: -6px;
        left: 50%;
        margin-left: -8px;
        border-top-color: #222;
        border-top-style: solid;
        border-top-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom {
        margin-top: 10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom::before {
        border-bottom: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        top: -6px;
        left: 50%;
        margin-left: -8px;
        border-bottom-color: #222;
        border-bottom-style: solid;
        border-bottom-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left {
        margin-left: -10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left::before {
        border-left: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        right: -6px;
        top: 50%;
        margin-top: -4px;
        border-left-color: #222;
        border-left-style: solid;
        border-left-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right {
        margin-left: 10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right::before {
        border-right: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        left: -6px;
        top: 50%;
        margin-top: -4px;
        border-right-color: #222;
        border-right-style: solid;
        border-right-width: 6px;
    }
  A business site is a normal website with general information about your business opposed to a funnel that is made to convert visitors to do business opposed to a funnel that is made to convert visitors to do something in kyvio you can manage your general site under ‘Smart Funnels’. This site is what visitors see if they visit auto-test-site.kyvio.icu or your custom domain. Funnels are always stored under sub-folders of your main domain404 page not found detectioncancelsave site settings</value>
      <webElementGuid>7475a4c2-d221-47e8-924f-49ca25dec89f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;wrapper d-flex&quot;]/div[@class=&quot;content&quot;]/main[1]/div[2]/div[@class=&quot;ab-content section-padding pt-5 pb-5&quot;]</value>
      <webElementGuid>fb086622-63e3-470a-9909-07b05f381870</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[3]/main/div[2]/div[2]</value>
      <webElementGuid>98d12492-c6a4-4b24-878d-ba05982e36e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[1]/following::div[5]</value>
      <webElementGuid>50f55b35-2d54-4dd9-aa81-e38c4a6020e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]</value>
      <webElementGuid>ab5ac07b-b8ac-49fc-b81c-c18bd2e93afe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'site statuspublishedunpublishedsite nameSite URL*This site URL can not be changed, you can set your own custom URL using Domain Mapping in the menu above.site logoe.g. https://domain.com/logo-image.pngbrowse / uploadfavicone.g. https://domain.com/favicon.icobrowse / uploadGoogle Sitemap XMLe.g. https://domain.com/sitemap.xmluploadManage Business siteClick here to manage your business site
  	.t5cac70be-c10e-40a6-ba6d-802f2d51e3e6 {
	    color: #fff;
	    background: #222;
	    border: 1px solid transparent;
  	}

  	.t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top {
        margin-top: -10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top::before {
        border-top: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        bottom: -6px;
        left: 50%;
        margin-left: -8px;
        border-top-color: #222;
        border-top-style: solid;
        border-top-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom {
        margin-top: 10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom::before {
        border-bottom: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        top: -6px;
        left: 50%;
        margin-left: -8px;
        border-bottom-color: #222;
        border-bottom-style: solid;
        border-bottom-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left {
        margin-left: -10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left::before {
        border-left: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        right: -6px;
        top: 50%;
        margin-top: -4px;
        border-left-color: #222;
        border-left-style: solid;
        border-left-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right {
        margin-left: 10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right::before {
        border-right: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        left: -6px;
        top: 50%;
        margin-top: -4px;
        border-right-color: #222;
        border-right-style: solid;
        border-right-width: 6px;
    }
  A business site is a normal website with general information about your business opposed to a funnel that is made to convert visitors to do business opposed to a funnel that is made to convert visitors to do something in kyvio you can manage your general site under ‘Smart Funnels’. This site is what visitors see if they visit auto-test-site.kyvio.icu or your custom domain. Funnels are always stored under sub-folders of your main domain404 page not found detectioncancelsave site settings' or . = 'site statuspublishedunpublishedsite nameSite URL*This site URL can not be changed, you can set your own custom URL using Domain Mapping in the menu above.site logoe.g. https://domain.com/logo-image.pngbrowse / uploadfavicone.g. https://domain.com/favicon.icobrowse / uploadGoogle Sitemap XMLe.g. https://domain.com/sitemap.xmluploadManage Business siteClick here to manage your business site
  	.t5cac70be-c10e-40a6-ba6d-802f2d51e3e6 {
	    color: #fff;
	    background: #222;
	    border: 1px solid transparent;
  	}

  	.t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top {
        margin-top: -10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top::before {
        border-top: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-top::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        bottom: -6px;
        left: 50%;
        margin-left: -8px;
        border-top-color: #222;
        border-top-style: solid;
        border-top-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom {
        margin-top: 10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom::before {
        border-bottom: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-bottom::after {
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        top: -6px;
        left: 50%;
        margin-left: -8px;
        border-bottom-color: #222;
        border-bottom-style: solid;
        border-bottom-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left {
        margin-left: -10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left::before {
        border-left: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-left::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        right: -6px;
        top: 50%;
        margin-top: -4px;
        border-left-color: #222;
        border-left-style: solid;
        border-left-width: 6px;
    }

    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right {
        margin-left: 10px;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right::before {
        border-right: 8px solid transparent;
    }
    .t5cac70be-c10e-40a6-ba6d-802f2d51e3e6.place-right::after {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        left: -6px;
        top: 50%;
        margin-top: -4px;
        border-right-color: #222;
        border-right-style: solid;
        border-right-width: 6px;
    }
  A business site is a normal website with general information about your business opposed to a funnel that is made to convert visitors to do business opposed to a funnel that is made to convert visitors to do something in kyvio you can manage your general site under ‘Smart Funnels’. This site is what visitors see if they visit auto-test-site.kyvio.icu or your custom domain. Funnels are always stored under sub-folders of your main domain404 page not found detectioncancelsave site settings')]</value>
      <webElementGuid>8505c711-6ca3-4ee6-963f-38dd28b06335</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
