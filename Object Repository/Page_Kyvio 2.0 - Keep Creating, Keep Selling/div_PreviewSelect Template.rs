<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_PreviewSelect Template</name>
   <tag></tag>
   <elementGuidId>ea4d6d03-4500-42dd-9c5f-44410656aa75</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.overlay</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[9]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>72430b48-39c3-4cbc-8898-722507324067</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>overlay</value>
      <webElementGuid>8a35598d-edaf-4e4f-afa3-fba7775cebef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PreviewSelect Template</value>
      <webElementGuid>b3a2216c-55c6-4e07-b3f9-32b8db41c457</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog create-funnel-modal&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;d-flex align-items-start justify-content-center&quot;]/div[@class=&quot;styles_content__wfhlb&quot;]/div[@class=&quot;styles_wrapper__5kwRF&quot;]/div[@class=&quot;styles_body__e43Pz modal-body&quot;]/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;row width-100&quot;]/div[@class=&quot;col-lg-4 col-md-4 col-xs-6 template funnel-template mb-4 false&quot;]/div[@class=&quot;d-block text-center pos-relative&quot;]/div[@class=&quot;box-shadow radius-sm  mb-3 thumbnail&quot;]/div[@class=&quot;overlay&quot;]</value>
      <webElementGuid>31e2af94-6c84-4d54-b0a8-c0ca0a344c25</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::div[9]</value>
      <webElementGuid>3e03b537-56e0-4fb2-adef-06f20fd0a080</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose Funnel Template'])[1]/following::div[9]</value>
      <webElementGuid>949b05d7-14c3-4f01-bf47-f6bf0fb259ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div/div/div/div</value>
      <webElementGuid>ee44f20d-760a-4054-8f52-2d8eda9f6908</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'PreviewSelect Template' or . = 'PreviewSelect Template')]</value>
      <webElementGuid>3109bc2c-8c52-4e72-b4ec-b2484dbb0dfa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
